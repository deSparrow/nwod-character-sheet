package master.sparrow.nwodcharactersheet.ui.characters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import master.sparrow.nwodcharactersheet.data.repositories.CharactersRepository;
import master.sparrow.nwodcharactersheet.data.models.Character;

import static java.util.Collections.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class CharactersActivityPresenterTest {
    @Mock
    private CharactersRepository charactersRepository;

    @Mock
    private CharactersActivityView view;

    private CharactersActivityPresenter presenter;
    private final List<Character> list = Arrays.asList(new Character(), new Character(), new Character());

    @Before
    public void setUp(){
        presenter = new CharactersActivityPresenter(view, charactersRepository);
    }

    @Test
    public void shouldFetchAndPassCharactersToView(){
        when(charactersRepository.getCharacters()).thenReturn(list);

        presenter.loadCharacters();

        verify(charactersRepository).getCharacters();
        verify(view).displayCharacters(list);
    }

    @Test
    public void shouldFetchAndDisplayNoCharacters(){
        when(charactersRepository.getCharacters()).thenReturn(EMPTY_LIST);

        presenter.loadCharacters();

        verify(charactersRepository).getCharacters();
        verify(view).displayNoCharacters();
    }

    @Test
    public void shouldInsertToDatabaseAndDisplayCreatedCharacterInView(){
        Character character = mock(Character.class);
        when(charactersRepository.insertCharacter(character)).thenReturn(character);

        presenter.createCharacter(character);

        verify(charactersRepository).insertCharacter(character);
        verify(view).displayCharacter(character);
    }

    @Test
    public void shouldDeleteCharacterFromDatabaseAndClearCharacterFromView(){
        Character character = mock(Character.class);

        presenter.deleteCharacter(character);

        verify(charactersRepository).deleteCharacter(character);
        verify(view).clearCharacterFromView(character);
    }

}