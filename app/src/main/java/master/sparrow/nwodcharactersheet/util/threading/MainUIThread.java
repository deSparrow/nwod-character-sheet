package master.sparrow.nwodcharactersheet.util.threading;

import android.os.Handler;
import android.os.Looper;

public class MainUIThread {
    private static MainUIThread INSTANCE;
    private Handler handler;

    private MainUIThread(){
        handler = new Handler(Looper.getMainLooper());
    }

    public static synchronized MainUIThread getInstance(){
        if(INSTANCE == null){
            INSTANCE = new MainUIThread();
        }
        return INSTANCE;
    }

    public void post(Runnable runnable){
        handler.post(runnable);
    }
}
