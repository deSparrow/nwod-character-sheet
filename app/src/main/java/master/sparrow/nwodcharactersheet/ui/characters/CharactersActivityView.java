package master.sparrow.nwodcharactersheet.ui.characters;

import java.util.List;

import master.sparrow.nwodcharactersheet.data.models.Character;

public interface CharactersActivityView {
    void displayCharacters(List<Character> list);
    void displayCharacter(Character character);
    void displayNoCharacters();
    void clearCharacterFromView(Character character);
}
