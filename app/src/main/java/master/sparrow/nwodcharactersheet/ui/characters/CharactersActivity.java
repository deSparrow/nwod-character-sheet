package master.sparrow.nwodcharactersheet.ui.characters;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import master.sparrow.nwodcharactersheet.R;
import master.sparrow.nwodcharactersheet.data.repositories.CharactersRepository;
import master.sparrow.nwodcharactersheet.data.repositories.DatabaseCharactersRepository;
import master.sparrow.nwodcharactersheet.data.models.Character;
import master.sparrow.nwodcharactersheet.util.ItemClickSupport;
import master.sparrow.nwodcharactersheet.util.threading.MainUIThread;
import master.sparrow.nwodcharactersheet.util.threading.ThreadExecutor;

public class CharactersActivity extends AppCompatActivity implements CharactersActivityView, CreateCharacterDialogListener, DeleteCharacterDialogListener{
    private CharactersActivityPresenter presenter;
    private CharactersRecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private FloatingActionButton floatingActionButton;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Activity methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters);

        recyclerView = findViewById(R.id.recycler_view_characters);
        floatingActionButton = findViewById(R.id.floating_action_button_add_character);
        List<Character> characters = new ArrayList<>();
        ThreadExecutor threadExecutor = ThreadExecutor.getInstance();
        MainUIThread mainUIThread = MainUIThread.getInstance();
        CharactersRepository repository = new DatabaseCharactersRepository(getApplicationContext());
        presenter = new CharactersActivityPresenter(this, repository);

        recyclerAdapter = new CharactersRecyclerAdapter(this, characters);
        recyclerView.setAdapter(recyclerAdapter);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0 ||dy<0 && floatingActionButton.isShown())
                    floatingActionButton.hide();
                else{
                    floatingActionButton.show();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    floatingActionButton.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener((recyclerView, position, event) -> {
            //ToDo go to another activity with info about target character
            Toast.makeText(getApplicationContext(), "Item clicked!", Toast.LENGTH_SHORT).show();
        });

        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener((recyclerView, position, event) -> {
            displayDeletionDialog(position);
            // recyclerAdapter.deleteCharacter(position);
            return true;
        });

        floatingActionButton.setOnClickListener(event -> {
            displayInsertionDialog();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.loadCharacters();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Private methods
    private void displayDeletionDialog(int position) {
        DeleteCharacterDialog dialog = new DeleteCharacterDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Character.CHARACTER_KEY, recyclerAdapter.getCharacter(position));
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, dialog.getTag());
    }

    private void displayInsertionDialog() {
        CreateCharacterDialog dialog = new CreateCharacterDialog();
        dialog.show(fragmentManager, dialog.getTag());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //View methods
    @Override
    public void displayCharacters(List<Character> list) {
        recyclerAdapter.clearCharacters();
        recyclerAdapter.addCharacters(list);
    }

    @Override
    public void displayCharacter(Character character){
        recyclerAdapter.addCharacter(character);
    }

    @Override
    public void displayNoCharacters() {
        //ToDo display no characters
    }

    @Override
    public void clearCharacterFromView(Character character) {
        recyclerAdapter.deleteCharacter(character);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Dialog listeners methods
    @Override
    public void addNewCharacter(Character character) {
        presenter.createCharacter(character);
    }

    @Override
    public void deleteCharacter(Character character) {
        presenter.deleteCharacter(character);
    }
}
