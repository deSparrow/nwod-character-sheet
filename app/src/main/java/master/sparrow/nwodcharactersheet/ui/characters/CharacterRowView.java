package master.sparrow.nwodcharactersheet.ui.characters;

interface CharacterRowView {
    void setCharacterName(String name);
    void setCharacterConcept(String concept);
    void setCharacterPicture(String filename);
}
