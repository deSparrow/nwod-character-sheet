package master.sparrow.nwodcharactersheet.ui.characters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import master.sparrow.nwodcharactersheet.R;
import master.sparrow.nwodcharactersheet.data.models.Character;

public class DeleteCharacterDialog extends DialogFragment {
    private View content;
    private CircleImageView imageViewPicture;
    private TextView textViewName;
    private TextView textViewConcept;
    private Character character;

    public DeleteCharacterDialog() {}

    private DeleteCharacterDialogListener dialogListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            dialogListener = (DeleteCharacterDialogListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            character = bundle.getParcelable(Character.CHARACTER_KEY);
        }
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = layoutInflater.inflate(R.layout.fragment_dialog_character_delete,null);
        imageViewPicture = content.findViewById(R.id.image_view_delete_character);
        textViewName = content.findViewById(R.id.text_view_delete_character_name);
        textViewConcept = content.findViewById(R.id.text_view_delete_character_concept);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        textViewName.setText(character.getName());
        textViewConcept.setText(character.getConcept());
        String pictureName = character.getPictureName();
        if(pictureName!=null){
            File img = new File(pictureName);
            if(img.exists()){
                imageViewPicture.setImageURI(Uri.parse(pictureName));
            }
        }
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_characters_delete_title)
                .setPositiveButton(R.string.dialog_characters_delete_positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialogListener.deleteCharacter(character);
                        File picture = new File(character.getPictureName());
                        if(picture.exists()){
                            picture.delete();
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_characters_delete_negative, null)
                .setView(content)
                .create();
    }
}