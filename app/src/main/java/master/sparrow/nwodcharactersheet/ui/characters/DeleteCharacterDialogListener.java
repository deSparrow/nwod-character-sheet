package master.sparrow.nwodcharactersheet.ui.characters;

import master.sparrow.nwodcharactersheet.data.models.Character;

public interface DeleteCharacterDialogListener {
    void deleteCharacter(Character character);
}
