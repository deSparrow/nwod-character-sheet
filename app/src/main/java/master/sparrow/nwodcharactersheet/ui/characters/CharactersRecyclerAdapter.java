package master.sparrow.nwodcharactersheet.ui.characters;

import master.sparrow.nwodcharactersheet.R;
import master.sparrow.nwodcharactersheet.data.models.Character;

import android.app.Activity;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class CharactersRecyclerAdapter extends RecyclerView.Adapter<CharactersRecyclerAdapter.CharacterViewHolder> {
    private List<Character> characters;
    private Activity activity;

    public CharactersRecyclerAdapter(Activity activity, List<Character> characters) {
        this.activity = activity;
        this.characters = characters;
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CharacterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_character_view, parent, false));
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder viewHolder, int position) {
        Character character = characters.get(position);
        viewHolder.characterNameTextView.setText(character.getName());
        viewHolder.characterConceptTextView.setText(character.getConcept());
        String pictureName = character.getPictureName();
        if(pictureName!=null){
            File img = new File(pictureName);
            if(img.exists()){
                viewHolder.characterPictureImageView.setImageURI(Uri.parse(pictureName));
            }
        }
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }

    public void clearCharacters(){
        int size = getItemCount();
        this.characters.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void deleteCharacter(Character character){
        int pos = characters.indexOf(character);
        characters.remove(character);
        notifyItemRemoved(pos);
    }

    public void addCharacter(Character character){
        characters.add(character);
        notifyItemInserted(getItemCount()-1);
    }

    public void addCharacters(List<Character> characters){
        int size = getItemCount();
        this.characters.addAll(characters);
        notifyItemRangeInserted(size, characters.size());
    }

    public Character getCharacter(int position) {
        return characters.get(position);
    }

    public static class CharacterViewHolder extends RecyclerView.ViewHolder{
        TextView characterNameTextView;
        TextView characterConceptTextView;
        ImageView characterPictureImageView;

        public CharacterViewHolder(View itemView) {
            super(itemView);
            characterNameTextView = itemView.findViewById(R.id.row_character_name);
            characterConceptTextView = itemView.findViewById(R.id.row_character_concept);
            characterPictureImageView = itemView.findViewById(R.id.row_character_picture);
        }
    }
}
