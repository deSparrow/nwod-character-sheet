package master.sparrow.nwodcharactersheet.ui.characters;

import android.support.v4.app.DialogFragment;

import master.sparrow.nwodcharactersheet.data.models.Character;

public interface CreateCharacterDialogListener{
    void addNewCharacter(Character character);
}