package master.sparrow.nwodcharactersheet.ui.characters;

import java.util.List;

import master.sparrow.nwodcharactersheet.data.models.Character;
import master.sparrow.nwodcharactersheet.data.repositories.CharactersRepository;


public class CharactersActivityPresenter {
    private CharactersActivityView view;
    private CharactersRepository charactersRepository;

    public CharactersActivityPresenter(CharactersActivityView view, CharactersRepository charactersRepository) {
        this.view = view;
        this.charactersRepository = charactersRepository;
    }

    public void loadCharacters(){
        if(view==null){
            return;
        }
        //ToDo progress bar start
        List<Character> characters = charactersRepository.getCharacters();//ToDO callback
        if(characters.isEmpty()){
            view.displayNoCharacters();
        } else{
            view.displayCharacters(characters);
        }
    }

    public void deleteCharacter(Character character) {
        charactersRepository.deleteCharacter(character);
        view.clearCharacterFromView(character);
    }

    public void createCharacter(Character character){
        Character createdCharacter = charactersRepository.insertCharacter(character);
        view.displayCharacter(createdCharacter);
    }
}
