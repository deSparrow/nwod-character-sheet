package master.sparrow.nwodcharactersheet.ui.characters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import master.sparrow.nwodcharactersheet.R;
import master.sparrow.nwodcharactersheet.data.models.Character;
import master.sparrow.nwodcharactersheet.data.models.converters.GenderConverter;

public class CreateCharacterDialog extends DialogFragment {
    private static final int PICK_GALLERY_IMAGE = 1;
    public static final String DATE_FORMAT = "yyyyMMdd_HHmmss";
    public static final String IMAGE_DIRECTORY = "nWODdata";
    private View content;
    private CircleImageView imageViewPicture;
    private EditText editTextName;
    private EditText editTextConcept;
    private RadioGroup radioGroupGender;
    private File file;
    private File sourceFile;
    private File destFile;
    private SimpleDateFormat dateFormatter;

    public CreateCharacterDialog() {}

    private CreateCharacterDialogListener dialogListener;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PICK_GALLERY_IMAGE:
                    Uri uriPhoto = data.getData();
                    imageViewPicture.setImageURI(uriPhoto);
                    sourceFile = new File(getPathFromGooglePhotosUri(uriPhoto));
                    destFile = new File(file, "img_"+ dateFormatter.format(new Date()) + ".png");
                    break;
            }
        }
    }

    public String getPathFromGooglePhotosUri(Uri uriPhoto) {
        if (uriPhoto == null)
            return null;

        FileInputStream input = null;
        FileOutputStream output = null;
        try {
            ParcelFileDescriptor pfd = getActivity().getContentResolver().openFileDescriptor(uriPhoto, "r");
            FileDescriptor fd = pfd.getFileDescriptor();
            input = new FileInputStream(fd);

            String tempFilename = getTempFilename(getContext());
            output = new FileOutputStream(tempFilename);

            int read;
            byte[] bytes = new byte[4096];
            while ((read = input.read(bytes)) != -1) {
                output.write(bytes, 0, read);
            }
            return tempFilename;
        } catch (IOException ignored) {
            // Nothing we can do
        } finally {
            closeSilently(input);
            closeSilently(output);
        }
        return null;
    }

    private static String getTempFilename(Context context) throws IOException {
        File outputDir = context.getCacheDir();
        File outputFile = File.createTempFile("image", "tmp", outputDir);
        return outputFile.getAbsolutePath();
    }

    private String copyFileAndReturnPath(File sourceFile, File destFile) throws IOException {
        if(sourceFile == null){
            return "";
        }
        if (!sourceFile.exists()) {
            return "";
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
        return destFile.getPath();
    }

    public static void closeSilently(Closeable c) {
        if (c == null)
            return;
        try {
            c.close();
        } catch (Throwable t) {
            // Do nothing
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            dialogListener = (CreateCharacterDialogListener)context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = layoutInflater.inflate(R.layout.fragment_dialog_character_create,null);
        imageViewPicture = content.findViewById(R.id.image_view_create_character);
        editTextName = content.findViewById(R.id.edit_text_create_character_name);
        editTextConcept = content.findViewById(R.id.edit_text_create_character_concept);
        radioGroupGender = content.findViewById(R.id.radio_group_create_character_gender);
        imageViewPicture.setOnClickListener(event -> {
            Intent intentPick = new Intent(Intent.ACTION_PICK);
            intentPick.setType("image/*");
            startActivityForResult(intentPick, PICK_GALLERY_IMAGE);
        });
        file = new File(getContext().getFilesDir() + "/" + IMAGE_DIRECTORY);
        if (!file.exists()) {
            file.mkdirs();
        }

        dateFormatter = new SimpleDateFormat(
                DATE_FORMAT, Locale.US);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_characters_create_title)
                .setPositiveButton(R.string.dialog_characters_create_positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Character character = new Character();
                        character.setName(editTextName.getText().toString());
                        character.setConcept(editTextConcept.getText().toString());
                        int genderValue = radioGroupGender.indexOfChild(content.findViewById(radioGroupGender.getCheckedRadioButtonId()));
                        character.setGender(GenderConverter.getGender(genderValue));
                        try {
                            character.setPictureName(copyFileAndReturnPath(sourceFile, destFile));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        dialogListener.addNewCharacter(character);
                    }
                })
                .setNegativeButton(R.string.dialog_characters_create_negative, null)
                .setView(content)
                .create();
    }
}
