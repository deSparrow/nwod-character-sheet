package master.sparrow.nwodcharactersheet.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import master.sparrow.nwodcharactersheet.data.models.Character;


@Dao
public interface CharacterDao {
    @Query("SELECT * FROM Characters")
    List<Character> getAll();

    @Update
    void update(Character character);

    @Insert
    long insert(Character character);

    @Delete
    void delete(Character character);
}