package master.sparrow.nwodcharactersheet.data.models.converters;

import android.arch.persistence.room.TypeConverter;

import master.sparrow.nwodcharactersheet.data.models.Character.Gender;

import static master.sparrow.nwodcharactersheet.data.models.Character.Gender.FEMALE;
import static master.sparrow.nwodcharactersheet.data.models.Character.Gender.MALE;

public class GenderConverter {
    @TypeConverter
    public static Gender getGender(int genderValue){
        if(genderValue == MALE.getGenderValue()){
            return MALE;
        } else{
            return FEMALE;
        }
    }

    @TypeConverter
    public static int getGenderValue(Gender gender){
        return gender.getGenderValue();
    }
}
