package master.sparrow.nwodcharactersheet.data.repositories;

import java.util.List;

import master.sparrow.nwodcharactersheet.data.models.Character;


public interface CharactersRepository {
    List<Character> getCharacters();
    void deleteCharacter(Character character);
    Character insertCharacter(Character character);
}
