package master.sparrow.nwodcharactersheet.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;


import lombok.Getter;
import lombok.Setter;
import master.sparrow.nwodcharactersheet.data.models.converters.GenderConverter;

@Entity(tableName = "Characters")
public class Character implements Parcelable {
    public static final String CHARACTER_KEY = "data.models.CHARACTER_KEY";

    public enum Gender {
        MALE(0),
        FEMALE(1);

        private int genderValue;

        Gender(int genderValue){
            this.genderValue = genderValue;
        }

        public int getGenderValue(){
            return genderValue;
        }
    }

    public Character(){}

    @PrimaryKey(autoGenerate = true)
    @Getter @Setter
    private int id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private int age;

    @TypeConverters(GenderConverter.class)
    @Getter @Setter
    private Gender gender = Gender.MALE;

    @Getter @Setter
    private String player;

    @Getter @Setter
    private String concept;

    @Getter @Setter
    private String virtue;

    @Getter @Setter
    private String vice;

    @Getter @Setter
    private String chronicle;

    @Getter @Setter
    private String faction;

    @Getter @Setter
    private String groupName;

    @Getter @Setter
    private int size;

    @Getter @Setter
    private int experience;

    @Getter @Setter
    private int bashingDamage;

    @Getter @Setter
    private int lethalDamage;

    @Getter @Setter
    private int aggravatedDamage;

    @Getter @Setter
    private int currentWillpower;

    @Getter @Setter
    private String notes;

    @Getter @Setter
    private String pictureName;

    protected Character(Parcel in) {
        id = in.readInt();
        name = in.readString();
        age = in.readInt();
        gender = Gender.values()[in.readInt()];
        player = in.readString();
        concept = in.readString();
        virtue = in.readString();
        vice = in.readString();
        chronicle = in.readString();
        faction = in.readString();
        groupName = in.readString();
        size = in.readInt();
        experience = in.readInt();
        bashingDamage = in.readInt();
        lethalDamage = in.readInt();
        aggravatedDamage = in.readInt();
        currentWillpower = in.readInt();
        notes = in.readString();
        pictureName = in.readString();
    }

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeInt(age);
        parcel.writeInt(gender.ordinal());
        parcel.writeString(player);
        parcel.writeString(concept);
        parcel.writeString(virtue);
        parcel.writeString(vice);
        parcel.writeString(chronicle);
        parcel.writeString(faction);
        parcel.writeString(groupName);
        parcel.writeInt(size);
        parcel.writeInt(experience);
        parcel.writeInt(bashingDamage);
        parcel.writeInt(lethalDamage);
        parcel.writeInt(aggravatedDamage);
        parcel.writeInt(currentWillpower);
        parcel.writeString(notes);
        parcel.writeString(pictureName);
    }
}
