package master.sparrow.nwodcharactersheet.data.repositories;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import master.sparrow.nwodcharactersheet.data.local.AppDatabase;
import master.sparrow.nwodcharactersheet.data.local.CharacterDao;
import master.sparrow.nwodcharactersheet.data.models.Character;


public class DatabaseCharactersRepository implements CharactersRepository {
    private CharacterDao characterDao;

    public DatabaseCharactersRepository(Context context) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        characterDao = appDatabase.characterDao();
    }

    @Override
    public List<Character> getCharacters() {
        return characterDao.getAll();
    }

    @Override
    public void deleteCharacter(Character character) {
        characterDao.delete(character);
    }

    @Override
    public Character insertCharacter(Character character) {
        long insertedCharacterId = characterDao.insert(character);
        character.setId((int) insertedCharacterId);
        return character;
    }
}
